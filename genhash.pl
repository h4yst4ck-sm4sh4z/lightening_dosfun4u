#!/usr/bin/perl
use Digest::SHA qw/sha1_hex/;

for ($a = 0; $x <= 255; $a ++) {
	for ($b = 0; $b <= 255; $b ++) {
		for ($c = 0; $c <= 255; $c ++) {
			for ($d = 0; $d <= 255; $d ++) {
				for ($e = 0; $e <= 255; $e ++) {
					for ($f = 0; $f <= 255; $f ++) {
						$input = "ALEBEGHJKBEKOAAA";
						$input .= chr($a);
						$input .= chr($b);
						$input .= chr($c);
						$input .= chr($d);
						$input .= chr($e);
						$input .= chr($f);
						$hash = sha1_hex($input);
						if ($hash =~ /^000000/) {
							print $input . "\n";
							exit();
						}
					}
				}
			}
		}
	}
}
